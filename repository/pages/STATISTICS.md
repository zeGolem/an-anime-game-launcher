# Usage statistics

This file is a launcher usage statistics archive. You can see here which journey we made to make the launcher looks like it looks

Our current statistics you can find in [readme](../../README.md)

> You can suggest colors for your countries

## 2.7.0

Start date: May 31, 2022

| Period | Data |
| - | - |
| 17 May — 31 May | <img src="../pics/stats/2022/may17-may31.png" height="400px"> |
| 26 May — 9 June | <img src="../pics/stats/2022/may26-jun9.png" height="400px"> |
| 13 June — 27 June | <img src="../pics/stats/2022/jun13-jun27.png" height="400px"> |

### Launcher analytics (by users choice)

<img src="../pics/stats/2.7.0.png">

<br>

## 2.6.0

| Period | Data |
| - | - |
| 6 Apr — 20 Apr | <img src="../pics/stats/2022/apr6-apr20.png" height="400px"> |
| 20 Apr — 2 May | <img src="../pics/stats/2022/apr20-may2.png" height="400px"> |
| 10 May — 22 May | <img src="../pics/stats/2022/may10-may22.png" height="400px"> |

### Launcher analytics (by users choice)

<img src="../pics/stats/2.6.0.png">

<br>

## 2.5.0

| Period | Data |
| - | - |
| 24 Feb — 8 Mar | <img src="../pics/stats/2022/feb24-mar8.png" height="400px"> |
| 16 Mar — Mar 30 | <img src="../pics/stats/2022/mar16-mar30.png" height="400px"> |

### Launcher analytics (by users choice)

<img src="../pics/stats/2.5.0.png">

<br>

## In-launcher analytics (without discord included)

> Note that this statistics was gathered only from newcomers, so it actually means 154 new users of the launcher

<br>

## 2.3.0 — 99 total

<img src="../pics/stats/2.3.0.png">

## 2.2.0 — 29 total

<img src="../pics/stats/2.2.0.png">
